-- Find all artists that has letter d in its name.
SELECT *
FROM artists
WHERE name 
LIKE "%d%";

-- Find all songs that has a length of less than 230.
SELECT *
FROM songs
WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length
FROM songs   
LEFT JOIN albums 
ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name, songs.length
FROM songs   
JOIN albums 
ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT *
FROM albums
LEFT JOIN artists 
ON artists.id = albums.artist_id
WHERE album_title
LIKE "%a%";

SELECT *
FROM albums
JOIN artists 
ON artists.id = albums.artist_id
WHERE album_title
LIKE "%a%";

SELECT *
FROM artists
JOIN albums
ON albums.artist_id = artists.id
WHERE album_title
LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT *
FROM albums
ORDER BY album_title DESC
LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
SELECT *
FROM albums   
LEFT JOIN songs 
ON songs.album_id = albums.id
ORDER BY album_title DESC, song_name ASC;

SELECT *
FROM albums   
JOIN songs 
ON songs.album_id = albums.id
ORDER BY album_title DESC, song_name ASC;